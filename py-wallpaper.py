from datetime import datetime, timedelta
from PIL import Image, ImageDraw, ImageFont  # https://pypi.org/project/Pillow/
import numbers
import os  # delete file
import sys  # exit if time near 00:00
import ctypes  # https://docs.python.org/3/library/ctypes.html
import wget  # get image from the web
import config

# ---Variables---
cameras = config.cameras() # get locations
path = "C:/Temp/"
location = config.location() # get number from config
name = cameras[location][0].split('/')[-2]
date = datetime.now().replace(second=0, microsecond=0)

if int(date.strftime('%M')[1:]) <= 2: # take previous image, give server time to upload
    date = date - timedelta(minutes=10)

if int(date.strftime('%H')) > 19 or int(date.strftime('%H')) < 6: # 30 min steps
    while not (int(date.strftime('%M')) == 0 or int(date.strftime('%M')) == 30):
        date = date - timedelta(minutes=1)
else: # 10 min steps
    while not int(date.strftime('%M')) % 10 == 0:
        date = date - timedelta(minutes=1)

# concatenate image URL & paths
pathconfig = path + name + "_webcam_config.txt"
pathimage = path + name + "_webcam.jpg"
datestring = date.strftime('%Y/%m/%d/%H%M')
image_url = cameras[location][0] + datestring + cameras[location][1]

if os.path.isfile(pathconfig):  # check if txt exists
    if datestring == open(pathconfig).read():
        print("no new picture available")
        sys.exit()

open(pathconfig, 'w').write(datestring)

# check if image exist and delete it
if os.path.exists(pathimage):
    os.remove(pathimage)

# download the new image and save it in pathimage folder
print("downloading from: " + image_url)
wget.download(image_url, pathimage)

# ---Edit image---
img = Image.open(pathimage)
# Call draw method to add 2D graphics in an image
img_draw = ImageDraw.Draw(img)
# Custom font style and font size
myFont = ImageFont.truetype(r"C:\Windows\Fonts\arial.ttf", 55)
img_draw.text((10, 10), name + " : " + date.strftime('%Y-%m-%d %H:%M'),
              fill=(0, 0, 255), font=myFont, align="right")  # Add Text to an image

# img.show()  # Display edited image
img.save(pathimage)  # Save the edited image (overwrite)

# ---Set walpaper in windows---
SPI_SETDESKWALLPAPER = 20  # varible for ctypes
# last varible defines fit of the walpaper; 1 = fill
ctypes.windll.user32.SystemParametersInfoW(
    SPI_SETDESKWALLPAPER, 0, pathimage, 1)
# https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-systemparametersinfow
