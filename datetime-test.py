from datetime import datetime, timedelta

date = datetime.now().replace(second=0, microsecond=0)

if int(date.strftime('%M')[1:]) <= 2: # take previous image, give server time to upload
    date = date - timedelta(minutes=10)

if int(date.strftime('%H')) > 19 or int(date.strftime('%H')) < 6: # 30 min steps
    while not (int(date.strftime('%M')) == 0 or int(date.strftime('%M')) == 30):
        date = date - timedelta(minutes=1)
else: # 10 min steps
    while not int(date.strftime('%M')) % 10 == 0:
        date = date - timedelta(minutes=1)

print(date)

#print(0 % 10 == 0)
#% True
