import requests
import os

from config import camera_ip, username, password

# Replace these with your actual camera's information
camera_ip = camera_ip
username = username
password = password

# API endpoint for capturing a snapshot
snapshot_url = f"http://{camera_ip}/cgi-bin/api.cgi?cmd=Snap&channel=0&rs=wuuPhkmUCeI9WG7v&user={username}&password={password}"

# Destination folder to save the downloaded picture
download_folder = "C:/Temp/"
os.makedirs(download_folder, exist_ok=True)

# Send a GET request to capture the snapshot
response = requests.get(snapshot_url)

if response.status_code == 200:
    # Determine the file name based on the timestamp
    filename = os.path.join(download_folder, "snapshot.jpg")

    # Save the picture to the specified file
    with open(filename, "wb") as f:
        f.write(response.content)
    print("Snapshot downloaded successfully.")
else:
    print("Failed to download snapshot.")