def location():
    return 0 # location chosen

# Reolink camera settings
camera_ip = "YOUR CONFIG HERE"
username = "YOUR USERNAME HERE"
password = "YOUR PASSWORD HERE"

def cameras():
    return [
        ["https://www.foto-webcam.eu/webcam/kampenwand/", "_uh.jpg"],
        ["https://www.foto-webcam.eu/webcam/hochries-ost/", "_uh.jpg"],
        ["https://www.foto-webcam.eu/webcam/bad-endorf/", "_uh.jpg"],
        ["https://www.foto-webcam.eu/webcam/rosenheim/", "_uh.jpg"],
        ["https://www.foto-webcam.eu/webcam/tum-olympiapark/", "_uh.jpg"],
        ["https://www.terra-hd.de/chiemsee/", "l.jpg"],
        ["https://www.terra-hd.de/bernau-felden/", "l.jpg"],
        ["https://www.terra-hd.de/prienavera/", "l.jpg"],
        ["https://www.terra-hd.de/domberg1/", "l.jpg"],
        ["https://www.terra-hd.de/deutschesmuseum2/", "l.jpg"]
    ]
